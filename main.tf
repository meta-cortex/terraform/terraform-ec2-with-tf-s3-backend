terraform {
  backend "s3" {
    bucket         = "terraform-state-bnet"
    key            = "terraform-ec2-s3-backend/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform_state"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"

  default_tags {
    tags = {
      environment = "test"
      source      = "https://gitlab.com/meta-cortex/terraform/terraform-ec2-with-tf-s3-backend"
      project     = "terraform ec2 with s3 backend"
      terraform   = "true"
    }
  }
}



resource "aws_instance" "app_server" {
  ami           = "ami-0b0af3577fe5e3532"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}